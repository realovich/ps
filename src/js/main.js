$(document).ready(function(){
//filter
$('.filt-btn').click(function(){
	$(this).parents('.filt-item').toggleClass('open').parent().siblings().find('.filt-item').removeClass('open');
});

$('.filt-clear').click(function(){
	$(this).parents('.filt-item').removeClass('active');
});

//-/filter

//SELECT
$('.my-select__current').click(function(e) {
  $(this).parents('.my-select').toggleClass('open');
});

$('.my-select__link').click(function(e) {
  e.preventDefault();
  var currentLabel = $(this).text();
  $('.my-select__link').removeClass('current');
  $(this).addClass('current');
  $('.my-select__current').text(currentLabel);
  $(this).parents('.my-select').toggleClass('open');
});
//-/SELECT
});
